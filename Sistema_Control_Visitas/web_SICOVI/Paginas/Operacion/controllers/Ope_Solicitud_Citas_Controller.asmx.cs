﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;
using datos_cambios_procesos;

using LitJson;
using Newtonsoft.Json;

namespace web_cambios_procesos.Paginas.Operacion.controllers
{
    /// <summary>
    /// Descripción breve de Ope_Solicitud_Citas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Solicitud_Citas_Controller : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
           Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> lst_Detalles_Horarios = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);
               lst_Detalles_Horarios = JsonConvert.DeserializeObject<List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>>(Obj_Solicitud.Datos_Detalles_Horarios);


                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _solicitud = new Ope_Solicitud_Citas();


                    if (Obj_Solicitud.Empleado_ID != null && Obj_Solicitud.Empleado_ID != 0)
                        _solicitud.Empleado_ID = Obj_Solicitud.Empleado_ID;
                    if (Obj_Solicitud.Empleado_Visitado != null && Obj_Solicitud.Empleado_Visitado != "")
                      _solicitud.Empleado_Visitado = Obj_Solicitud.Empleado_Visitado;
                    _solicitud.Departamento_ID = Obj_Solicitud.Departamento_ID;
                    _solicitud.Estatus_ID = Obj_Solicitud.Estatus_ID;
                    _solicitud.Nombre_Solicitante = Obj_Solicitud.Nombre_Solicitante;
                    _solicitud.Apellidos = Obj_Solicitud.Apellidos;
                    _solicitud.Correo = Obj_Solicitud.Correo;
                    _solicitud.Telefono = Obj_Solicitud.Telefono;
                    _solicitud.Motivo_Cita = Obj_Solicitud.Motivo_Cita;
                    _solicitud.Usuario_Creo = Cls_Sesiones.Usuario;
                    _solicitud.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                   Ope_Solicitud_Citas solicitud = dbContext.Ope_Solicitud_Citas.Add(_solicitud);

                    foreach (var Detalles_Horarios in lst_Detalles_Horarios)
                    {
                        Ope_Horarios_Solicitud_Detalles det_horarios = new Ope_Horarios_Solicitud_Detalles();
                        det_horarios.No_Solicitud = solicitud.No_Solicitud;
                        det_horarios.Fecha_Cita = Detalles_Horarios.Fecha_Cita;
                        det_horarios.Hora_Inicio = Detalles_Horarios.Hora_Inicio;
                        det_horarios.Hora_Fin = Detalles_Horarios.Hora_Fin;
                        det_horarios.Aprobado = "NO";
                        dbContext.Ope_Horarios_Solicitud_Detalles.Add(det_horarios);

                    }
 
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    // Mensaje.Mensaje = "La operación se completó sin problemassssss."+ _solicitud.No_Solicitud;
                    Mensaje.Mensaje = "<i class='fa fa-warning' style = 'color:#f2041a; font-size:30px;' ></i > &nbsp;<h3 style = 'color:#f2041a; font-size:20px;'> Número de Solicitud asignado:" + _solicitud.No_Solicitud + "</h3></br> Guarda este número </br>Es muy importante para dar seguimento a tu solicitud"; ;
                    //Mensaje.Mensaje = "<i class='fa fa-warning' style='font - size:100px; color:#f2041a;'></i>&nbsp;Número de Solicitud asignado: " + _solicitud.No_Solicitud + "</br> Guarda este número </br>Es muy importante para dar serguimento a tu solicitud ";
    }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> lst_Detalles_Horarios = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);
              //  lst_Detalles_Horarios = JsonConvert.DeserializeObject<List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>>(Obj_Solicitud.Datos_Detalles_Horarios);
               // List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
               
                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud == Obj_Solicitud.No_Solicitud).First();

                    if (Obj_Solicitud.Empleado_ID != null && Obj_Solicitud.Empleado_ID != 0)
                        _solicitud.Empleado_ID = Obj_Solicitud.Empleado_ID;
                    if (Obj_Solicitud.Empleado_Visitado != null && Obj_Solicitud.Empleado_Visitado != "")
                        _solicitud.Empleado_Visitado = Obj_Solicitud.Empleado_Visitado;
                    _solicitud.Departamento_ID = Obj_Solicitud.Departamento_ID;
                    _solicitud.Estatus_ID = Obj_Solicitud.Estatus_ID;
                    _solicitud.Nombre_Solicitante = Obj_Solicitud.Nombre_Solicitante;
                    _solicitud.Apellidos = Obj_Solicitud.Apellidos;
                    _solicitud.Correo = Obj_Solicitud.Correo;
                    _solicitud.Telefono = Obj_Solicitud.Telefono;
                    _solicitud.Motivo_Cita = Obj_Solicitud.Motivo_Cita;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    // Mensaje.Mensaje = "La operación se completó sin problemassssss."+ _solicitud.No_Solicitud;
                    Mensaje.Mensaje = "<i class='fa fa-warning' style = 'color:#f2041a; font-size:30px;' ></i > &nbsp;Modificación exitosa.Número de Solicitud asignado:<h1 style = 'color:#f2041a; font-size:20px;'>" + _solicitud.No_Solicitud + "</h1>"; 
                    //Mensaje.Mensaje = "<i class='fa fa-warning' style='font - size:100px; color:#f2041a;'></i>&nbsp;Número de Solicitud asignado: " + _solicitud.No_Solicitud + "</br> Guarda este número </br>Es muy importante para dar serguimento a tu solicitud ";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitud_Por_Filtros(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitud = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
          
                    var citas = (from _cita in dbContext.Ope_Solicitud_Citas
                                 join _estatus in dbContext.Apl_Estatus on _cita.Estatus_ID equals _estatus.Estatus_ID
                                 join _departamento in dbContext.Cat_Departamentos on _cita.Departamento_ID equals _departamento.Departamento_ID
                                 //join _empleados in dbContext.Cat_Empleados on _cita.Empleado_ID equals _empleados.Empleado_ID
                                 //join _horarios in dbContext.Ope_Horarios_Solicitud_Detalles on _cita.No_Solicitud equals _horarios.No_Solicitud
                                 from _empleados in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _cita.Empleado_ID).DefaultIfEmpty()
                                 where
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Solicitud.Nombre_Solicitante) ? _cita.Nombre_Solicitante.ToLower().Contains(Obj_Solicitud.Nombre_Solicitante.ToLower()) : true)

                                 select new Cls_Ope_Solicitud_Citas_Negocio
                                 {
                                     No_Solicitud = _cita.No_Solicitud,
                                     Nombre_Solicitante = _cita.Nombre_Solicitante + " " + _cita.Apellidos,

                                     //Empleado_Visitado = _empleados.Nombre + " " + _empleados.Apellidos,
                                     Empleado_ID=_cita.Empleado_ID,
                                     Departamento_ID=_cita.Departamento_ID,
                                     Empleado_Visitado = _cita.Empleado_Visitado != null ? _cita.Empleado_Visitado : _empleados.Nombre +" " + _empleados.Apellidos,
                                     Departamento = _departamento.Nombre,
                                     Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _cita.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault(),
                                     Hora_Inicio= (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _cita.No_Solicitud select subconsulta.Hora_Inicio.ToString()).FirstOrDefault(),
                                     //Hora_Inicio= (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _cita.No_Solicitud select Convert(DAT, FEC_FAC, 108), 8) AS FECHA FROM FACTURA ).FirstOrDefault(),
                                     Estatus_ID=_cita.Estatus_ID,
                                     Estatus = _estatus.Estatus
                                    
                                     //_areas.Fecha_Creo = new DateTime?(DateTime.Now).Value;



                }).OrderByDescending(u => u.No_Solicitud);



                    foreach (var p in citas)
                        Lista_solicitud.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitud);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Departamentos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Departamentos = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "ACTIVO").First();

                    var _lst_departamentos = (from _departamentos in dbContext.Cat_Departamentos
                                              where
                                             _departamentos.Nombre.Contains(q)
                                              select new Cls_Select2
                                              {
                                                  id = _departamentos.Departamento_ID.ToString(),
                                                  text = _departamentos.Nombre,
                                                  tag = String.Empty
                                              }).OrderBy(u => u.text);

                    if (_lst_departamentos.Any())
                        foreach (var p in _lst_departamentos)
                            Lista_Departamentos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Empleados()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Departamentos = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _lst_empleados = (from _empleados in dbContext.Cat_Empleados

                                              where _empleados.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                               && _empleados.Estatus=="ACTIVO"
                                              && _empleados.Nombre.Contains(q)
                                              select new Cls_Select2
                                              {
                                                  id = _empleados.Empleado_ID.ToString(),
                                                  text = _empleados.Nombre + " "+_empleados.Apellidos,
                                                  tag = String.Empty
                                              }).OrderBy(u => u.text);

                    if (_lst_empleados.Any())
                        foreach (var p in _lst_empleados)
                            Lista_Departamentos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Datos_Departamentos(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = new Cls_Cat_Empleados_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _lst_empleado = (from _empleado in dbContext.Cat_Empleados
                                         join _departamento in dbContext.Cat_Departamentos on _empleado.Departamento_ID equals _departamento.Departamento_ID

                                         where
                                         _empleado.Estatus == "ACTIVO"
                                         && _empleado.Departamento_ID == Obj_Empleados.Departamento_ID

                                         select new Cls_Cat_Empleados_Negocio
                                         {
                                            
                                             Empleado_ID=_empleado.Empleado_ID,
                                             No_Empleado = _empleado.No_Empleado,
                                             Empleado = _empleado.Nombre + " " + _empleado.Apellidos,
                                             Departamento_ID=_departamento.Departamento_ID
                                             
                                         }).OrderBy(u => u.No_Empleado);

                    foreach (var p in _lst_empleado)
                        Lista_Empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Datos_Empleados(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = new Cls_Cat_Empleados_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _lst_empleado = (from _empleado in dbContext.Cat_Empleados
                                         join _departamento in dbContext.Cat_Departamentos on _empleado.Departamento_ID equals _departamento.Departamento_ID

                                         where
                                         _empleado.Estatus == "ACTIVO"
                                         && _empleado.Empleado_ID == Obj_Empleados.Empleado_ID

                                         select new Cls_Cat_Empleados_Negocio
                                         {

                                             Departamento_ID = _departamento.Departamento_ID,
                                             Departamento=_departamento.Nombre
                                          

                                         }).OrderBy(u => u.Departamento_ID);

                    foreach (var p in _lst_empleado)
                        Lista_Empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cancelar_Solicitud_Cita(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Cancelar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "CANCELADO").First();
                    var _Solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _Solicitud.Estatus_ID = _estatus.Estatus_ID;
                    _Solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _Solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; La Solicitud fue cancelada exitosamente." +" <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Observaciones(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Observaciones = Obj_Solicitud.Observaciones;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Solicitud rechazada." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Solicitud(string jsonObject)
        {
           Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitud = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    string n_departamento = Obj_Solicitud.Departamento_ID == null ? "" :
                        dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Solicitud.Departamento_ID).Select(u => u.Nombre).First();
                    string n_empleado = Obj_Solicitud.Empleado_ID == null ? "" :
                      dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Solicitud.Empleado_ID).Select(u => u.Nombre).First();
                    

                    var Alertas = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                  // join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                   //join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                   //from _empleados in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()
                                  
                                   where 
                                   _solicitud.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)

                                   select new Cls_Ope_Solicitud_Citas_Negocio
                                   {
                                       No_Solicitud= _solicitud.No_Solicitud,
                                       Nombre_Solicitante=_solicitud.Nombre_Solicitante,
                                       Apellidos=_solicitud.Apellidos,
                                       Correo=_solicitud.Correo,
                                      // Estatus=n_estatus,
                                       //Estatus_ID=_estatus.Estatus_ID,
                                      // Empleado_ID=_empleados.Empleado_ID,
                                       Telefono=_solicitud.Telefono,
                                       Departamento=n_departamento,
                                       Empleado_Visitado=n_empleado,
                                       //Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleados.Nombre + " " + _empleados.Apellidos,
                                       Motivo_Cita =_solicitud.Motivo_Cita,
                                      // Estatus=_estatus.Estatus
                                     
                                   }).OrderByDescending(u => u.No_Solicitud);
                    foreach (var p in Alertas)
                        Lista_solicitud.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitud);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Horarios(string jsonObject)
        {
            //Cls_Cambios_Correos_Negocio obj_Correos = null;
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio obj_horarios = null;
            string Json_Resultado = string.Empty;
            //List<Cls_Cambios_Correos_Negocio> Lista_Detalles = new List<Cls_Cambios_Correos_Negocio>();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles
                                           //join _departamento in dbContext.Cat_Departamentos on _detalle.Departamento_ID equals _departamento.Departamento_ID
                                           //join _empleados in dbContext.Cat_Empleados on _detalle.Empleado_ID equals _empleados.Empleado_ID

                                       where _detalle.No_Solicitud.Equals(obj_horarios.No_Solicitud)
                                  
                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {
                                           No_Solicitud= _detalle.No_Solicitud,
                                           Horario_Solicitud_Detalle_ID=_detalle.Horario_Solicitud_Detalle_ID,
                                           Fecha_Cita=_detalle.Fecha_Cita,
                                           Hora_Inicio=_detalle.Hora_Inicio,
                                           Hora_Fin=_detalle.Hora_Fin
                                   
                                       }).OrderByDescending(u => u.No_Solicitud);


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horario = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Horario = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _horarios = new Ope_Horarios_Solicitud_Detalles();
                    _horarios.No_Solicitud = Obj_Horario.No_Solicitud;
                    _horarios.Fecha_Cita = Obj_Horario.Fecha_Cita;
                    _horarios.Hora_Inicio = Obj_Horario.Hora_Inicio;
                    _horarios.Hora_Fin = Obj_Horario.Hora_Fin;
                    _horarios.Aprobado = "NO";

                    dbContext.Ope_Horarios_Solicitud_Detalles.Add(_horarios);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "Horario agregado exitosamente.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_alertas = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            DateTime fecha_cita = DateTime.MinValue;
            try

            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);
                if (!string.IsNullOrEmpty(Obj_Solicitud.Fecha_Cita))
                {
                    DateTime.TryParseExact(Obj_Solicitud.Fecha_Cita, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out fecha_cita);
                }

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                   join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                   join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                   from _empleado in dbContext.Cat_Empleados.Where(x=> x.Empleado_ID ==_solicitud.Empleado_ID).DefaultIfEmpty()
                                 //  from _detalles in dbContext.Ope_Horarios_Solicitud_Detalles.Where(x=> x.No_Solicitud == _solicitud.No_Solicitud).DefaultIfEmpty()

                                   where
                                   
                                    (Obj_Solicitud.No_Solicitud!=0 ? _solicitud.No_Solicitud==(Obj_Solicitud.No_Solicitud):true)&&
                                    (!string.IsNullOrEmpty(Obj_Solicitud.Nombre_Solicitante) ? _solicitud.Nombre_Solicitante.ToLower().Contains(Obj_Solicitud.Nombre_Solicitante.ToLower()): true)&&
                                    (!string.IsNullOrEmpty(Obj_Solicitud.Apellidos) ? _solicitud.Apellidos.ToLower().Contains(Obj_Solicitud.Apellidos.ToLower()) : true)
                                   //(((string.IsNullOrEmpty(Obj_Solicitud.Fecha_Cita)) && (!string.IsNullOrEmpty(Obj_Solicitud.Fecha_Cita))) ? (_detalles.Fecha_Cita == fecha_cita) : true)
                                   
                                   select new Cls_Ope_Solicitud_Citas_Negocio
                                   {
                                       No_Solicitud=_solicitud.No_Solicitud,
                                       Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleado.Nombre + " " + _empleado.Apellidos,
                                       Departamento= _departamento.Nombre,
                                       Nombre_Solicitante=_solicitud.Nombre_Solicitante + " " + _solicitud.Apellidos,
                                       //Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault(),
                                       Estatus = _estatus.Estatus,
                                       Departamento_ID=_solicitud.Departamento_ID,
                                       Estatus_ID=_solicitud.Estatus_ID,
                                       Empleado_ID=_solicitud.Empleado_ID,
                                       Correo=_solicitud.Correo,
                                       Telefono=_solicitud.Telefono,
                                       Motivo_Cita=_solicitud.Motivo_Cita
                                       
                                       
                                   }).OrderByDescending(u => u.No_Solicitud);

                    foreach (var p in Alertas)
                        Lista_alertas.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horario = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Horario = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _horario = dbContext.Ope_Horarios_Solicitud_Detalles.Where(u => u.Horario_Solicitud_Detalle_ID == Obj_Horario.Horario_Solicitud_Detalle_ID).First();
                    dbContext.Ope_Horarios_Solicitud_Detalles.Remove(_horario);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new Sistema_Control_Visitas_Entities())
                        {
                            var _horario = dbContext.Ope_Horarios_Solicitud_Detalles.Where(u => u.Horario_Solicitud_Detalle_ID == Obj_Horario.Horario_Solicitud_Detalle_ID).First();
                            var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud == Obj_Horario.No_Solicitud).First();

                            _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Validar_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            //List<Cls_Cambios_Supervisores_Negocio> Lista_Supervisores = new List<Cls_Cambios_Supervisores_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles

                                       where _detalle.No_Solicitud.Equals(Obj_Horarios.No_Solicitud) 

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           No_Solicitud = Obj_Horarios.No_Solicitud,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin,
                                           Aprobado = _detalle.Aprobado

                                       }).OrderByDescending(u => u.Horario_Solicitud_Detalle_ID);


                    if (lst_detalle.Any())
                    {
                        Mensaje.Mensaje = (true).ToString();
                    }
                    else
                    {
                        Mensaje.Mensaje = (false).ToString();
                    }
                    Json_Resultado = JsonMapper.ToJson(Mensaje);

                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

    }
}
