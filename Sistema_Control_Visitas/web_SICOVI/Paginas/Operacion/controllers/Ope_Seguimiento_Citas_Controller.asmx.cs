﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Operacion.controllers
{
    /// <summary>
    /// Descripción breve de Ope_Seguimiento_Citas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Seguimiento_Citas_Controller : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitudes(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_Solicitudes = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                   
                    var Solicitudes = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                       join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                       join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                       from _empleado in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()
                                       join _horario in dbContext.Ope_Horarios_Solicitud_Detalles on _solicitud.No_Solicitud equals _horario.No_Solicitud

                                       where
                                       _horario.Aprobado == "SI" &&
                                       _horario.Fecha_Cita == DateTime.Today

                                       select new Cls_Ope_Solicitud_Citas_Negocio
                                       {
                                           No_Solicitud = _solicitud.No_Solicitud,
                                           Empleado_ID = _solicitud.Empleado_ID,
                                           Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleado.Nombre + " " + _empleado.Apellidos,
                                           Estatus = _estatus.Estatus,
                                           Estatus_ID = _solicitud.Estatus_ID,
                                           Nombre_Solicitante = _solicitud.Nombre_Solicitante + " " + _solicitud.Apellidos,
                                           Departamento = _departamento.Nombre,
                                           Departamento_ID = _solicitud.Departamento_ID,
                                           Fecha_Cita = _horario.Fecha_Cita.ToString()
                                           //Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault()
                                           //Fecha_Cita=(from _horario in dbContext.Ope_Horarios_Solicitud_Detalles where _horario.No_Solicitud==_solicitud.No_Solicitud select ())
                                           //Aprobado=_detalles.Aprobado


                                       }).OrderByDescending(u => u.No_Solicitud);

                    foreach (var p in Solicitudes)
                        Lista_Solicitudes.Add((Models.Negocio.Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Solicitudes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Imagenes(string jsonObject)
        {
            Cls_Ope_Visitas_Imagenes Obj_Imagenes = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Visitas_Imagenes> Lista_Imagenes = new List<Cls_Ope_Visitas_Imagenes>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Imagenes = JsonMapper.ToObject<Cls_Ope_Visitas_Imagenes>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var Imagenes = (from _imagen in dbContext.Ope_Visitas_Imagenes
                                       where
                                       _imagen.Id_Solicitud == Obj_Imagenes.No_Solicitud

                                       select new Cls_Ope_Visitas_Imagenes
                                       {
                                           Id = _imagen.Id,
                                           No_Solicitud = _imagen.Id_Solicitud,
                                           Ruta_Externa = _imagen.Ruta_Externa

                                           //Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault()
                                           //Fecha_Cita=(from _horario in dbContext.Ope_Horarios_Solicitud_Detalles where _horario.No_Solicitud==_solicitud.No_Solicitud select ())
                                           //Aprobado=_detalles.Aprobado


                                       }).OrderByDescending(u => u.Id);

                    foreach (var p in Imagenes)
                        Lista_Imagenes.Add((Models.Negocio.Cls_Ope_Visitas_Imagenes)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Imagenes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitud = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    string n_departamento = Obj_Solicitud.Departamento_ID == null ? "" :
                        dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Solicitud.Departamento_ID).Select(u => u.Nombre).First();
                    string n_empleado = Obj_Solicitud.Empleado_ID == null ? "" :
                      dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Solicitud.Empleado_ID).Select(u => u.Nombre).First();


                    var Alertas = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                       // join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                       //join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                       //from _empleados in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()

                                   where
                                   _solicitud.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)

                                   select new Cls_Ope_Solicitud_Citas_Negocio
                                   {
                                       No_Solicitud = _solicitud.No_Solicitud,
                                       Nombre_Solicitante = _solicitud.Nombre_Solicitante,
                                       Apellidos = _solicitud.Apellidos,
                                       Correo = _solicitud.Correo,
                                       // Estatus=n_estatus,
                                       //Estatus_ID=_estatus.Estatus_ID,
                                       // Empleado_ID=_empleados.Empleado_ID,
                                       Telefono = _solicitud.Telefono,
                                       Departamento = n_departamento,
                                       Empleado_Visitado = n_empleado,
                                       Observaciones = _solicitud.Observaciones,
                                       //Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleados.Nombre + " " + _empleados.Apellidos,
                                       Motivo_Cita = _solicitud.Motivo_Cita,
                                       // Estatus=_estatus.Estatus

                                   }).OrderByDescending(u => u.No_Solicitud);
                    foreach (var p in Alertas)
                        Lista_solicitud.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitud);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Horarios(string jsonObject)
        {
            //Cls_Cambios_Correos_Negocio obj_Correos = null;
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio obj_horarios = null;
            string Json_Resultado = string.Empty;
            //List<Cls_Cambios_Correos_Negocio> Lista_Detalles = new List<Cls_Cambios_Correos_Negocio>();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles
                                           //join _departamento in dbContext.Cat_Departamentos on _detalle.Departamento_ID equals _departamento.Departamento_ID
                                           //join _empleados in dbContext.Cat_Empleados on _detalle.Empleado_ID equals _empleados.Empleado_ID

                                       where _detalle.No_Solicitud.Equals(obj_horarios.No_Solicitud)&&
                                       _detalle.Aprobado=="SI"

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {
                                           No_Solicitud = _detalle.No_Solicitud,
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin

                                       }).OrderByDescending(u => u.No_Solicitud);


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

     
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ruta(string jsonObject)
        {  
           Cls_Ope_Geolocalizacion_Negocio obj_Ruta = null;
           string Json_Resultado = string.Empty;
           List<Cls_Ope_Geolocalizacion_Negocio> Lista_Detalles = new List<Cls_Ope_Geolocalizacion_Negocio>();
           Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_Ruta = JsonMapper.ToObject<Cls_Ope_Geolocalizacion_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Geolocalizacion
                                       join _solicitud in dbContext.Ope_Solicitud_Citas on _detalle.Solicitud_ID equals _solicitud.No_Solicitud
                                       where _detalle.Solicitud_ID.Equals(obj_Ruta.Solicitud_ID)
                                                                          
                                       select new Cls_Ope_Geolocalizacion_Negocio
                                       {
                                          Solicitud_ID=_detalle.Solicitud_ID,
                                          Geolocalizacion_ID=_detalle.Geolocalizacion_ID,
                                          Latitud=_detalle.Latitud,
                                          Longitud=_detalle.Longitud

                                       }).OrderByDescending(u => u.Geolocalizacion_ID);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ruta_Regreso(string jsonObject)
        {
            Cls_Ope_Geolocalizacion_Negocio obj_Ruta = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Geolocalizacion_Negocio> Lista_Detalles = new List<Cls_Ope_Geolocalizacion_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_Ruta = JsonMapper.ToObject<Cls_Ope_Geolocalizacion_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Geolocalizacion 
                                       join _solicitud in dbContext.Ope_Solicitud_Citas on _detalle.Solicitud_ID equals _solicitud.No_Solicitud
                                       where _detalle.Solicitud_ID.Equals(obj_Ruta.Solicitud_ID) 
                              
                                       select new Cls_Ope_Geolocalizacion_Negocio
                                       {
                                           Solicitud_ID = _detalle.Solicitud_ID,
                                           Geolocalizacion_ID = _detalle.Geolocalizacion_ID,
                                           Latitud = _detalle.Latitud,
                                           Longitud = _detalle.Longitud

                                       }).OrderByDescending(u => u.Geolocalizacion_ID);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Finalizar_Visita(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Finalizar Visita";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _estatus_finalizado = dbContext.Apl_Estatus.Where(p => p.Estatus == "FINALIZADO").First();
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Estatus_ID = _estatus_finalizado.Estatus_ID;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);
                 
                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La visita fue finalizada exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

         [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitud = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
      
            DateTime fecha_inicio = DateTime.MinValue;
            DateTime fecha_termino = DateTime.MinValue;
            try

            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);
                if (!string.IsNullOrEmpty(Obj_Solicitud.Fecha_Cita))
                {
                    DateTime.TryParseExact(Obj_Solicitud.Fecha_Cita, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out fecha_inicio);
                }

                fecha_termino = fecha_inicio.AddDays(1);
                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                   join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                   join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                   from _empleado in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()
                                   join _horarios in dbContext.Ope_Horarios_Solicitud_Detalles on _solicitud.No_Solicitud equals _horarios.No_Solicitud
                                   //from _detalles in dbContext.Ope_Horarios_Solicitud_Detalles.Where(x => x.No_Solicitud == _solicitud.No_Solicitud).DefaultIfEmpty()

                                   where
                                   _solicitud.Estatus_ID != 2 &&
                                   _horarios.Aprobado == "SI" &&
                                    (Obj_Solicitud.No_Solicitud != 0 ? _solicitud.No_Solicitud == (Obj_Solicitud.No_Solicitud) : true) &&
                                    (!string.IsNullOrEmpty(Obj_Solicitud.Nombre_Solicitante) ? _solicitud.Nombre_Solicitante.ToLower().Contains(Obj_Solicitud.Nombre_Solicitante.ToLower()) : true) &&
                                    (!string.IsNullOrEmpty(Obj_Solicitud.Apellidos) ? _solicitud.Apellidos.ToLower().Contains(Obj_Solicitud.Apellidos.ToLower()) : true) &&
                                    (((!string.IsNullOrEmpty(Obj_Solicitud.Fecha_Cita))) ? ((_horarios.Fecha_Cita >= fecha_inicio) && (_horarios.Fecha_Cita < fecha_termino)) : true)
                                   select new Cls_Ope_Solicitud_Citas_Negocio
                                   {
                                       No_Solicitud = _solicitud.No_Solicitud,
                                       Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleado.Nombre + " " + _empleado.Apellidos,
                                       Departamento = _departamento.Nombre,
                                       Nombre_Solicitante = _solicitud.Nombre_Solicitante + " " + _solicitud.Apellidos,
                                       Fecha_Cita = _horarios.Fecha_Cita.ToString(),
                                      // Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault(),
                                       Estatus = _estatus.Estatus,
                                       Departamento_ID=_solicitud.Departamento_ID,
                                       Estatus_ID=_solicitud.Estatus_ID,
                                       Empleado_ID=_solicitud.Empleado_ID,
                                       Correo=_solicitud.Correo,
                                       Telefono=_solicitud.Telefono,
                                       Motivo_Cita=_solicitud.Motivo_Cita                       
                                   }).OrderByDescending(u => u.No_Solicitud);

                    foreach (var p in Alertas)
                        Lista_solicitud.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitud);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Horarios_Visualizar(string jsonObject)
        {
            //Cls_Cambios_Correos_Negocio obj_Correos = null;
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio obj_horarios = null;
            string Json_Resultado = string.Empty;
            //List<Cls_Cambios_Correos_Negocio> Lista_Detalles = new List<Cls_Cambios_Correos_Negocio>();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles
                                           //join _departamento in dbContext.Cat_Departamentos on _detalle.Departamento_ID equals _departamento.Departamento_ID
                                           //join _empleados in dbContext.Cat_Empleados on _detalle.Empleado_ID equals _empleados.Empleado_ID

                                       where _detalle.No_Solicitud.Equals(obj_horarios.No_Solicitud)

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {

                                           No_Solicitud = _detalle.No_Solicitud,
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin,
                                           Aprobado = _detalle.Aprobado

                                       }).OrderByDescending(u => u.No_Solicitud);


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ubicacion_Vigilancia(string jsonObject)
        {
            Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            List<Cls_Apl_Parametros_Movil_Negocio> Lista_Detalles = new List<Cls_Apl_Parametros_Movil_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {

                    var lst_detalle = (from _detalle in dbContext.Apl_Parametros_Movil

                                       select new Cls_Apl_Parametros_Movil_Negocio
                                       {
                                          Parametro_ID=_detalle.Parametro_ID,
                                          Latitud_Ubicacion_Empresa = (double)_detalle.Latitud_Ubicacion_Empresa,
                                          Longitud_Ubicacion_Empresa = (double)_detalle.Longitud_Ubicacion_Empresa,
                                          Latitud_Ubicacion_Vigilancia = (double)_detalle.Latitud_Ubicacion_Vigilancia,
                                          Longitud_Ubicacion_Vigilancia = (double)_detalle.Longitud_Ubicacion_Vigilancia 
                                          
                                       }).OrderByDescending(u => u.Parametro_ID);


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ubicacion_Visitado(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Visita = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_Detalles = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Visita = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);
           
                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {

                    var lst_detalle = (from _detalle in dbContext.Ope_Solicitud_Citas

                                       where _detalle.No_Solicitud == Obj_Visita.No_Solicitud

                                       select new Cls_Ope_Solicitud_Citas_Negocio
                                       {
                                           Latitud_Visitado = _detalle.Latitud_Visitado,
                                           Longitud_Visitado = _detalle.Longitud_Visitado

                                       });

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
    }
}
