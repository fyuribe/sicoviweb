﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using datos_cambios_procesos;
using web_cambios_procesos.Models.Negocio;
using LitJson;
using admin_cambios_procesos.Models.Ayudante;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace web_cambios_procesos.Paginas.Operacion.controllers
{
    /// <summary>
    /// Descripción breve de Ope_Autorizacion_Solicitud_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Autorizacion_Solicitud_Controller : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitudes_Por_Usuario(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_Solicitudes = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    //var fecha = dbContext.Ope_Horarios_Solicitud_Detalles.Select(x => x.Fecha_Cita).LastOrDefault();

                    //if (fecha != null) {
                    //    var val = dbContext.Ope_Horarios_Solicitud_Detalles.Max(x => x.Fecha_Cita);
                    //   };

                    var maxValue = dbContext.Ope_Horarios_Solicitud_Detalles.Max(x => x.Fecha_Cita);
                    var result = dbContext.Ope_Horarios_Solicitud_Detalles.First(x => x.Fecha_Cita == maxValue);

                    var Solicitudes = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                       join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                       join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                       from _empleado in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()
                                           // from _detalles in dbContext.Ope_Horarios_Solicitud_Detalles.Where(x => x.No_Solicitud == _solicitud.No_Solicitud).DefaultIfEmpty()

                                       where

                                       _estatus.Estatus != "DELETED" &&
                                       // _solicitud.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud) &&
                                       //_solicitud.Empleado_ID == Cls_Sesiones.Datos_Empleados.Empleado_ID  
                                       //quitar esta linea 
                                        _solicitud.Empleado_ID != null


                                       select new Cls_Ope_Solicitud_Citas_Negocio
                                       {
                                           No_Solicitud = _solicitud.No_Solicitud,
                                           Empleado_ID = _solicitud.Empleado_ID,
                                           Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleado.Nombre + " " + _empleado.Apellidos,
                                           Estatus = _estatus.Estatus,
                                           Estatus_ID = _solicitud.Estatus_ID,
                                           Nombre_Solicitante = _solicitud.Nombre_Solicitante + " " + _solicitud.Apellidos,
                                           Departamento = _departamento.Nombre,
                                           Departamento_ID = _solicitud.Departamento_ID,
                                           Observaciones=_solicitud.Observaciones

                                           //Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault()
                                           //Fecha_Cita=(from _horario in dbContext.Ope_Horarios_Solicitud_Detalles where _horario.No_Solicitud==_solicitud.No_Solicitud select ())
                                           //Aprobado=_detalles.Aprobado


                                       }).OrderByDescending(u => u.No_Solicitud);

                    foreach (var p in Solicitudes)
                        Lista_Solicitudes.Add((Models.Negocio.Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Solicitudes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Solicitudes_Por_Departamento(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitudes = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {

                    var Solicitudes = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                       join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                       join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                       from _empleado in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()
                                           // from _detalles in dbContext.Ope_Horarios_Solicitud_Detalles.Where(x => x.No_Solicitud == _solicitud.No_Solicitud).DefaultIfEmpty()

                                       where

                                       _estatus.Estatus != "DELETED"   &&                                                           
                                      // _solicitud.Departamento_ID == Cls_Sesiones.Datos_Empleados.Departamento_ID &&
                                       _solicitud.Empleado_ID == null

                                       // _solicitud.Nombre_Solicitante==Obj_Solicitud.Empleado_Visitado

                                       select new Cls_Ope_Solicitud_Citas_Negocio
                                       {
                                           No_Solicitud = _solicitud.No_Solicitud,
                                           Empleado_ID = _solicitud.Empleado_ID,
                                           Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleado.Nombre + " " + _empleado.Apellidos,
                                           Estatus = _estatus.Estatus,
                                           Estatus_ID = _solicitud.Estatus_ID,
                                           Nombre_Solicitante = _solicitud.Nombre_Solicitante + " " + _solicitud.Apellidos,
                                           Departamento = _departamento.Nombre,
                                           Departamento_ID = _solicitud.Departamento_ID,
                                           Observaciones=_solicitud.Observaciones
                                           //Fecha_Cita = (from subconsulta in dbContext.Ope_Horarios_Solicitud_Detalles where subconsulta.No_Solicitud == _solicitud.No_Solicitud select subconsulta.Fecha_Cita.ToString()).FirstOrDefault(),

                                           //Aprobado=_detalles.Aprobado


                                       }).OrderByDescending(u => u.No_Solicitud);

                    foreach (var p in Solicitudes)
                        Lista_solicitudes.Add((Models.Negocio.Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitudes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Horarios(string jsonObject)
        {
            //Cls_Cambios_Correos_Negocio obj_Correos = null;
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio obj_horarios = null;
            string Json_Resultado = string.Empty;
            //List<Cls_Cambios_Correos_Negocio> Lista_Detalles = new List<Cls_Cambios_Correos_Negocio>();
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles
                                           //join _departamento in dbContext.Cat_Departamentos on _detalle.Departamento_ID equals _departamento.Departamento_ID
                                           //join _empleados in dbContext.Cat_Empleados on _detalle.Empleado_ID equals _empleados.Empleado_ID

                                       where _detalle.No_Solicitud.Equals(obj_horarios.No_Solicitud)

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {

                                           No_Solicitud = _detalle.No_Solicitud,
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin,
                                           Aprobado = _detalle.Aprobado

                                       }).OrderByDescending(u => u.No_Solicitud);


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Autorizar_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horario = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Autorizar horario";
                Obj_Horario = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    // var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "CANCELADO").First();
                    //var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Horario.No_Solicitud)).First();
                    var _autorizar = dbContext.Ope_Horarios_Solicitud_Detalles.Where(x => x.Horario_Solicitud_Detalle_ID == Obj_Horario.Horario_Solicitud_Detalle_ID).First();
                    _autorizar.Aprobado = "SI";
                    // _solicitud.Estatus_ID = _estatus.Estatus_ID;
                    // _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    // _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; El horario fue autorizado exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Rechazar_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horario = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar horario";
                Obj_Horario = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    // var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "CANCELADO").First();
                    //var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Horario.No_Solicitud)).First();
                    var _autorizar = dbContext.Ope_Horarios_Solicitud_Detalles.Where(x => x.Horario_Solicitud_Detalle_ID == Obj_Horario.Horario_Solicitud_Detalle_ID).First();
                    _autorizar.Aprobado = "NO";
                    // _solicitud.Estatus_ID = _estatus.Estatus_ID;
                    // _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    // _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; El horario fue rechazado exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Horarios_Por_Filtros(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_horarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_horarios = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var citas = (from _horario in dbContext.Ope_Horarios_Solicitud_Detalles
                                 join _cita in dbContext.Ope_Solicitud_Citas on _horario.No_Solicitud equals _cita.No_Solicitud

                                 where _horario.No_Solicitud == Obj_horarios.No_Solicitud
                                 select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                 {
                                     Horario_Solicitud_Detalle_ID=_horario.Horario_Solicitud_Detalle_ID,
                                     No_Solicitud = _cita.No_Solicitud,
                                     Fecha_Cita = _horario.Fecha_Cita,
                                     Hora_Inicio = _horario.Hora_Inicio,
                                     Hora_Fin = _horario.Hora_Fin,
                                     Aprobado = _horario.Aprobado

                                 }).OrderByDescending(u => u.Fecha_Cita);
                    foreach (var p in citas)
                        Lista_horarios.Add((Cls_Ope_Horarios_Solicitud_Detalles_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_horarios);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Empleados()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Departamentos = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _lst_empleados = (from _empleados in dbContext.Cat_Empleados

                                          where _empleados.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID &&
                                        //  _empleados.Departamento_ID == Cls_Sesiones.Datos_Empleados.Departamento_ID
                                        //quitar esta linea 
                                        _empleados.Departamento_ID == 14

                                           && _empleados.Estatus == "ACTIVO"
                                          && _empleados.Nombre.Contains(q)
                                          select new Cls_Select2
                                          {
                                              id = _empleados.Empleado_ID.ToString(),
                                              text = _empleados.Nombre + " " + _empleados.Apellidos,
                                              tag = String.Empty
                                          }).OrderBy(u => u.text);

                    if (_lst_empleados.Any())
                        foreach (var p in _lst_empleados)
                            Lista_Departamentos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Rechazar_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _estatus_rechazado = dbContext.Apl_Estatus.Where(p => p.Estatus == "RECHAZADO").First();
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Estatus_ID = _estatus_rechazado.Estatus_ID;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);
                    _solicitud.Aprobado = "NO";
                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La solicitud fue rechazada exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Observaciones(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Observaciones = Obj_Solicitud.Observaciones;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Solicitud rechazada." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Validar_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            //List<Cls_Cambios_Supervisores_Negocio> Lista_Supervisores = new List<Cls_Cambios_Supervisores_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles

                                       where _detalle.No_Solicitud.Equals(Obj_Horarios.No_Solicitud) &&
                                               _detalle.Aprobado == "SI"

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           No_Solicitud = Obj_Horarios.No_Solicitud,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin,
                                           Aprobado = _detalle.Aprobado

                                       }).OrderByDescending(u => u.Horario_Solicitud_Detalle_ID);


                    if (lst_detalle.Any())
                    {
                        Mensaje.Mensaje = (true).ToString();
                    }
                    else
                    {
                        Mensaje.Mensaje = (false).ToString();
                    }
                    Json_Resultado = JsonMapper.ToJson(Mensaje);

                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Validar_Horario_Rechazado(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            //List<Cls_Cambios_Supervisores_Negocio> Lista_Supervisores = new List<Cls_Cambios_Supervisores_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Horarios = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    // var horario = true;
                    var lst_detalle = (from _detalle in dbContext.Ope_Horarios_Solicitud_Detalles

                                       where _detalle.No_Solicitud.Equals(Obj_Horarios.No_Solicitud)
                                       //_detalle.Aprobado == "NO"

                                       select new Cls_Ope_Horarios_Solicitud_Detalles_Negocio
                                       {
                                           Horario_Solicitud_Detalle_ID = _detalle.Horario_Solicitud_Detalle_ID,
                                           No_Solicitud = Obj_Horarios.No_Solicitud,
                                           Fecha_Cita = _detalle.Fecha_Cita,
                                           Hora_Inicio = _detalle.Hora_Inicio,
                                           Hora_Fin = _detalle.Hora_Fin,
                                           Aprobado = _detalle.Aprobado

                                       }).OrderByDescending(u => u.Horario_Solicitud_Detalle_ID);

                    foreach (var p in lst_detalle)
                    {
                        if (p.Aprobado == "NO")
                        {


                        }
                        else
                        {
                            Mensaje.Mensaje = (false).ToString();
                        }
                    }

                    Json_Resultado = JsonMapper.ToJson(Mensaje);

                }
            }

            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Autorizar_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Autorizar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _estatus_autorizado = dbContext.Apl_Estatus.Where(p => p.Estatus == "AUTORIZADO").First();
                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Estatus_ID = _estatus_autorizado.Estatus_ID;
                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);
                    _solicitud.Aprobado = "SI";
                    _solicitud.Empleado_ID = Obj_Solicitud.Empleado_ID;
                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La solicitud fue autorizada exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Asignar_Empleado_Cita(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Autorizar solicitud";
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {

                    var _solicitud = dbContext.Ope_Solicitud_Citas.Where(u => u.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)).First();

                    _solicitud.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _solicitud.Fecha_Modifico = new DateTime?(DateTime.Now);
                    _solicitud.Empleado_ID = Obj_Solicitud.Empleado_ID;
                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La solicitud fue autorizada exitosamente." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta_Horario(string jsonObject)
        {
            Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Horario = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Horario = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _horarios = new Ope_Horarios_Solicitud_Detalles();
                    _horarios.No_Solicitud = Obj_Horario.No_Solicitud;
                    _horarios.Fecha_Cita = Obj_Horario.Fecha_Cita;
                    _horarios.Hora_Inicio = Obj_Horario.Hora_Inicio;
                    _horarios.Hora_Fin = Obj_Horario.Hora_Fin;
                    _horarios.Aprobado = "SI";

                    dbContext.Ope_Horarios_Solicitud_Detalles.Add(_horarios);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;El horario agregado será asignado para la cita.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Horario(string jsonObject)
        {
           Cls_Ope_Horarios_Solicitud_Detalles_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio> Lista_Solicitudes = new List<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Horarios_Solicitud_Detalles_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {


                    var Horario = (from _horarios in dbContext.Ope_Horarios_Solicitud_Detalles
                                   where
                                   _horarios.No_Solicitud == Obj_Solicitud.No_Solicitud &&
                                   _horarios.Aprobado == "SI"
                                   select _horarios);



                    foreach (var p in Horario)
                        // Lista_Solicitudes.Add((Models.Negocio.Cls_Ope_Solicitud_Citas_Negocio)p);
                        p.Aprobado = "NO";
                    dbContext.SaveChanges();

                    //Json_Resultado = JsonMapper.ToJson(Lista_Solicitudes);
                }

                Mensaje.Estatus = "success";
                Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La solicitud fue autorizada exitosamente." + " <br />";
            }

            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Solicitud(string jsonObject)
        {
            Cls_Ope_Solicitud_Citas_Negocio Obj_Solicitud = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Solicitud_Citas_Negocio> Lista_solicitud = new List<Cls_Ope_Solicitud_Citas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Solicitud = JsonMapper.ToObject<Cls_Ope_Solicitud_Citas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    string n_departamento = Obj_Solicitud.Departamento_ID == null ? "" :
                        dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Solicitud.Departamento_ID).Select(u => u.Nombre).First();
                    string n_empleado = Obj_Solicitud.Empleado_ID == null ? "" :
                      dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Solicitud.Empleado_ID).Select(u => u.Nombre).First();


                    var Alertas = (from _solicitud in dbContext.Ope_Solicitud_Citas
                                       // join _estatus in dbContext.Apl_Estatus on _solicitud.Estatus_ID equals _estatus.Estatus_ID
                                       //join _departamento in dbContext.Cat_Departamentos on _solicitud.Departamento_ID equals _departamento.Departamento_ID
                                       //from _empleados in dbContext.Cat_Empleados.Where(x => x.Empleado_ID == _solicitud.Empleado_ID).DefaultIfEmpty()

                                   where
                                   _solicitud.No_Solicitud.Equals(Obj_Solicitud.No_Solicitud)

                                   select new Cls_Ope_Solicitud_Citas_Negocio
                                   {
                                       No_Solicitud = _solicitud.No_Solicitud,
                                       Nombre_Solicitante = _solicitud.Nombre_Solicitante,
                                       Apellidos = _solicitud.Apellidos,
                                       Correo = _solicitud.Correo,
                                       // Estatus=n_estatus,
                                       //Estatus_ID=_estatus.Estatus_ID,
                                       // Empleado_ID=_empleados.Empleado_ID,
                                       Telefono = _solicitud.Telefono,
                                       Departamento = n_departamento,
                                       Empleado_Visitado = n_empleado,
                                       Observaciones=_solicitud.Observaciones,
                                       //Empleado_Visitado = _solicitud.Empleado_Visitado != null ? _solicitud.Empleado_Visitado : _empleados.Nombre + " " + _empleados.Apellidos,
                                       Motivo_Cita = _solicitud.Motivo_Cita,
                                       // Estatus=_estatus.Estatus

                                   }).OrderByDescending(u => u.No_Solicitud);
                    foreach (var p in Alertas)
                        Lista_solicitud.Add((Cls_Ope_Solicitud_Citas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_solicitud);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
    }
}