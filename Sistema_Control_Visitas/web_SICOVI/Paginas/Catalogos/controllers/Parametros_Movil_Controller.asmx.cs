﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Descripción breve de Parametros_Movil_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Parametros_Movil_Controller : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
         
            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);

               
                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _parametros = new Apl_Parametros_Movil();

                    _parametros.Estatus_ID = Obj_Parametros.Estatus_ID;
                    _parametros.Ruta_Servicios_Web = Obj_Parametros.Ruta_Servicios_Web;
                    _parametros.Latitud_Ubicacion_Empresa =Convert.ToDecimal( Obj_Parametros.Latitud_Ubicacion_Empresa);
                    _parametros.Longitud_Ubicacion_Empresa =Convert.ToDecimal( Obj_Parametros.Longitud_Ubicacion_Empresa);
                    _parametros.Latitud_Ubicacion_Vigilancia =Convert.ToDecimal( Obj_Parametros.Latitud_Ubicacion_Vigilancia);
                    _parametros.Longitud_Ubicacion_Vigilancia =Convert.ToDecimal( Obj_Parametros.Longitud_Ubicacion_Vigilancia);
                    _parametros.Distancia_Vigilancia = Obj_Parametros.Distancia_Vigilancia;
                    _parametros.Tiempo_Peticiones_GPS = Obj_Parametros.Tiempo_Peticiones_GPS;
                    _parametros.Aplicacion_ID = Obj_Parametros.Aplicacion_ID;
                    _parametros.Remitente_ID = Obj_Parametros.Remitente_ID;
                    _parametros.Api_Token = Obj_Parametros.Api_Token;
                    _parametros.Perfil_Seguridad = Obj_Parametros.Perfil_Seguridad;
          
                    dbContext.Apl_Parametros_Movil.Add(_parametros);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);



                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _parametros = dbContext.Apl_Parametros_Movil.Where(u => u.Parametro_ID == Obj_Parametros.Parametro_ID).First();

                    _parametros.Estatus_ID = Obj_Parametros.Estatus_ID;
                    _parametros.Ruta_Servicios_Web = Obj_Parametros.Ruta_Servicios_Web;
                    _parametros.Latitud_Ubicacion_Empresa =Convert.ToDecimal( Obj_Parametros.Latitud_Ubicacion_Empresa);
                    _parametros.Longitud_Ubicacion_Empresa =Convert.ToDecimal( Obj_Parametros.Longitud_Ubicacion_Empresa);
                    _parametros.Latitud_Ubicacion_Vigilancia =Convert.ToDecimal( Obj_Parametros.Latitud_Ubicacion_Vigilancia);
                    _parametros.Longitud_Ubicacion_Vigilancia =Convert.ToDecimal( Obj_Parametros.Longitud_Ubicacion_Vigilancia);
                    _parametros.Distancia_Vigilancia = Obj_Parametros.Distancia_Vigilancia;
                    _parametros.Tiempo_Peticiones_GPS = Obj_Parametros.Tiempo_Peticiones_GPS;
                    _parametros.Aplicacion_ID = Obj_Parametros.Aplicacion_ID;
                    _parametros.Remitente_ID = Obj_Parametros.Remitente_ID;
                    _parametros.Api_Token = Obj_Parametros.Api_Token;
                    _parametros.Perfil_Seguridad = Obj_Parametros.Perfil_Seguridad;


                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var _parametros = dbContext.Apl_Parametros_Movil.Where(u => u.Parametro_ID == Obj_Parametros.Parametro_ID).First();
                    dbContext.Apl_Parametros_Movil.Remove(_parametros);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new Sistema_Control_Visitas_Entities())
                        {
                            var _parametro = dbContext.Apl_Parametros_Movil.Where(u => u.Parametro_ID == Obj_Parametros.Parametro_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _parametro.Estatus_ID = _estatus.Estatus_ID;
                         

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametros_Por_Nombre(string jsonObject)
        {
          Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            List<Cls_Apl_Parametros_Movil_Negocio> Lista_Parametros = new List<Cls_Apl_Parametros_Movil_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            //Double latitud_empresa = (double)Obj_Parametros.Latitud_Ubicacion_Empresa;
            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                   
                    var _parametros = (from _parametro in dbContext.Apl_Parametros_Movil
                                  join _estatus in dbContext.Apl_Estatus on _parametro.Estatus_ID equals _estatus.Estatus_ID

                                  where _parametro.Parametro_ID.Equals(Obj_Parametros.Parametro_ID) 
                                 
                                  select new Cls_Apl_Parametros_Movil_Negocio
                                  {
                                    
                                      Parametro_ID=_parametro.Parametro_ID,
                                      Estatus = _estatus.Estatus,
                                      Ruta_Servicios_Web=_parametro.Ruta_Servicios_Web,
                                      Latitud_Ubicacion_Empresa = (double)_parametro.Latitud_Ubicacion_Empresa,
                                      Longitud_Ubicacion_Empresa = (double)_parametro.Longitud_Ubicacion_Empresa,
                                      Latitud_Ubicacion_Vigilancia =(double) _parametro.Latitud_Ubicacion_Vigilancia,
                                      Longitud_Ubicacion_Vigilancia = (double)_parametro.Longitud_Ubicacion_Vigilancia,
                                      Distancia_Vigilancia = _parametro.Distancia_Vigilancia,
                                      Tiempo_Peticiones_GPS = _parametro.Tiempo_Peticiones_GPS,
                                      Aplicacion_ID=_parametro.Aplicacion_ID,
                                      Remitente_ID=_parametro.Remitente_ID,
                                      Api_Token=_parametro.Api_Token,
                                      Perfil_Seguridad=_parametro.Perfil_Seguridad
                                      
                                  }).OrderByDescending(u => u.Parametro_ID);

                    if (_parametros.Any())
                    {
                        if (Obj_Parametros.Parametro_ID== 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Parametros.Ruta_Servicios_Web))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _parametros.Where(u => u.Parametro_ID == Obj_Parametros.Parametro_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Parametros.Ruta_Servicios_Web))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {

            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametros_Por_Filtros(string jsonObject)
        {
            Cls_Apl_Parametros_Movil_Negocio Obj_Parametros = null;
            string Json_Resultado = string.Empty;
            List<Cls_Apl_Parametros_Movil_Negocio> Lista_parametros = new List<Cls_Apl_Parametros_Movil_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Parametros = JsonMapper.ToObject<Cls_Apl_Parametros_Movil_Negocio>(jsonObject);

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Parametros = (from _parametro in dbContext.Apl_Parametros_Movil
                                 join _estatus in dbContext.Apl_Estatus on _parametro.Estatus_ID equals _estatus.Estatus_ID

                                 where 
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Parametros.Ruta_Servicios_Web) ? _parametro.Ruta_Servicios_Web.ToLower().Contains(Obj_Parametros.Ruta_Servicios_Web.ToLower()) : true)

                                 select new Cls_Apl_Parametros_Movil_Negocio
                                 {
                                     Parametro_ID =_parametro.Parametro_ID,

                                     Estatus = _estatus.Estatus,
                                     Estatus_ID=_parametro.Estatus_ID,
                                     Ruta_Servicios_Web = _parametro.Ruta_Servicios_Web,
                                     Latitud_Ubicacion_Empresa =(double)_parametro.Latitud_Ubicacion_Empresa,
                                     Longitud_Ubicacion_Empresa = (double)_parametro.Longitud_Ubicacion_Empresa,
                                     Latitud_Ubicacion_Vigilancia =(double) _parametro.Latitud_Ubicacion_Vigilancia,
                                     Longitud_Ubicacion_Vigilancia = (double)_parametro.Longitud_Ubicacion_Vigilancia,
                                     Distancia_Vigilancia = _parametro.Distancia_Vigilancia,
                                     Tiempo_Peticiones_GPS = _parametro.Tiempo_Peticiones_GPS,
                                     Aplicacion_ID = _parametro.Aplicacion_ID,
                                     Remitente_ID = _parametro.Remitente_ID,
                                     Api_Token = _parametro.Api_Token,
                                     Perfil_Seguridad = _parametro.Perfil_Seguridad

                                 }).OrderByDescending(u => u.Parametro_ID);



                    foreach (var p in Parametros)
                        Lista_parametros.Add((Cls_Apl_Parametros_Movil_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_parametros);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new Sistema_Control_Visitas_Entities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

    }
}
