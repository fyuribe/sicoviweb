﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Visitantes_Negocio
    {
        public int Visitante_ID { get; set; }
        public int Estatus_ID { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }
        public string Usuario_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public DateTime Fecha_Modifico { get; set; }

    }
}