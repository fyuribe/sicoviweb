﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Ope_Horarios_Solicitud_Detalles_Negocio
    {
        public int Horario_Solicitud_Detalle_ID { set; get; }
        public int No_Solicitud { set; get; }
        public DateTime Fecha_Cita { set; get; }
        public DateTime Hora_Inicio { get; set; }
        public DateTime Hora_Fin { get; set; }
        public string Aprobado { get; set; }
    }
}