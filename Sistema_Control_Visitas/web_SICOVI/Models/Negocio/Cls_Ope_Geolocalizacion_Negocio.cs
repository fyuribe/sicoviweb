﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Ope_Geolocalizacion_Negocio
    {
        public int Geolocalizacion_ID { get; set; }
        public int Solicitud_ID { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
    }
}