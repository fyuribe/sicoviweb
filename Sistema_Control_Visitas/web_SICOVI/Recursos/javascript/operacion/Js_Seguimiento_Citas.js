﻿
var estatusActivo = '';
var $table_horarios = null;
var $table = null;
var mapa;
var numero_foto = 0;
$(document).on('ready', function () {
    _inicializar_pagina();
    $table_horarios = $('#tbl_lista_horarios');
    _set_location_toolbar();
   
});
function _inicializar_pagina() {
    try {
        _estado_inicial();
        _crear_tbl_horarios(); 
        _crear_tbl_citas();
        setInterval(_search_citas, 60000);
        _search_citas();
        _load_estatus();
        _set_location_toolbar();
        _eventos();

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _habilitar_controles(opcion) {
    $('#div_principal_solicitud_citas').css('display', 'none');
    $('#div_Informacion').css('display', 'none');
    switch (opcion) {
        case 'Inicial':
            $('#div_principal_solicitud_citas').css('display', 'block');
            break;
        case 'Nuevo':
            $('#div_Informacion').css('display', 'block');
            $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            break;
        case 'Modificar':
            $('#div_Informacion').css('display', 'block');
            // $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            $('#cmb_estatus').attr("disabled", true);
            break;
        case 'Autorizar':

            //$('#cmb_empleado_visitado').attr("disabled",true);
            $('#cmb_departamento_visitado').attr("disabled", true);
            $('#cmb_estatus').attr("disabled", true);
            $('#div_Informacion').css('display', 'block');
            $('#nombre_solicitante').attr("disabled", true);
            $('#apellidos_solicitante').attr("disabled", true);
            $('#correo_solicitante').attr("disabled", true);
            $('#telefono_solicitante').attr("disabled", true);
            $('#txt_motivo_cita').attr("disabled", true);
            //$('#txt_no_solicitud').atrr("disabled", true);
            //$('#tbl_lista_horarios').bootstrapTable('hideColumn', 'Autorizar');
            break;
        case 'Visualizar':
            $('#cmb_empleado_visitado').attr("disabled", true);
            $('#cmb_departamento_visitado').attr("disabled", true);
            $('#cmb_estatus').attr("disabled", true);
            $('#div_Informacion').css('display', 'block');
            $('#nombre_solicitante').attr("disabled", true);
            $('#apellidos_solicitante').attr("disabled", true);
            $('#correo_solicitante').attr("disabled", true);
            $('#telefono_solicitante').attr("disabled", true);
            $('#txt_motivo_cita').attr("disabled", true);
            break;
    }
}

function _eventos() {
    try {
    
        $('#btn_inicio').on('click', function (e) { e.preventDefault(); window.location.href = '../Operacion/Frm_Ope_Seguimiento_Citas.aspx'; });
        $('#btn_busqueda').on('click', function (e) {
            _search_solicitudes_por_filtros();
        });
        $('#btn_salir').on('click', function (e) {

            $('#div_Informacion').css('display', 'none');
            $('#div_principal_solicitud_citas').css('display', 'block');
            _limpiar_controles();
        });
        $('#btn_guardar_solicitud').on('click', function (e) {
            e.preventDefault();
            bootbox.confirm({
                title: 'Capturar Solicitud Cita',
                message: '¿Está seguro de realizar la captura de la solicitud?',
                callback: function (result) {
                    if (result) {
                        var output = _validar_datos();
                        if (output.Estatus) {

                            if ($table_horarios.bootstrapTable('getOptions').totalRows < 1) {
                                _mostrar_mensaje('Validación', 'Se debe agregar por lo menos una propuesta de horarios.');
                                return;
                            }

                            if ($('#txt_no_solicitud').val() == '')

                                _guardar_solicitud_cita();

                            else {

                                _modificar_solicitud_cita();
                                _limpiar_controles();
                                $('#div_Informacion').css('display', 'none');
                                $('#div_principal_solicitud_citas').css('display', 'block');
                                //_crear_tbl_avisos_consulta();
                                //_search();
                            }
                        } else _mostrar_mensaje('Validación', output.Mensaje);
                    }
                }
            });
        });
        $('#btn_regresar').on('click', function (e) {
            e.preventDefault();  
            window.location.href = '../Operacion/Frm_Ope_Seguimiento_Citas.aspx';
        })
        $('#btn_volver').on('click', function (e) {
            e.preventDefault();
            window.location.href = '../Operacion/Frm_Ope_Seguimiento_Citas.aspx';
        })


        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
        $('#modal_datos select').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}
function _estado_inicial() {
    try {
        $('#dtp_fecha_cita').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_cita").datetimepicker("useCurrent", true);

        $('#dtp_fecha_busqueda').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_busqueda").datetimepicker("useCurrent", true);

        $('#dtp_hora_inicio').datetimepicker({
            //format: "LT"
            format: "hh:mm"
        });
        $('#dtp_hora_inicio').datetimepicker("useCurrent", true);

        $('#dtp_hora_fin').datetimepicker({

            format: "hh:mm"
        });
        $("#dtp_hora_fin").datetimepicker("useCurrent", true);




    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;nico", ex.message)
    }
}


function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observations</span><br />';
    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}


function _search_citas() {
    var filtros = null;
    try {
        filtros = new Object();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Solicitudes',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null && datos.d !== '') {
                    $('#tbl_citas').bootstrapTable('load', JSON.parse(datos.d));
                    // _agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    datos_cb = JSON.parse(datos.d);
        
                    for (var indice = 0; indice < datos_cb.length; indice++) {
                        if (datos_cb[indice].Estatus == "CONCLUIDO") {
                            _mostrar_mensaje('Finalizar visita', 'La visita con No. de solicitud: ' + datos_cb[indice].No_Solicitud + ' ha sido concluida ahora puedes finalizar esta visita.')
                        }

                    }
                
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}

function _crear_tbl_citas() {
    try {
        $('#tbl_citas').bootstrapTable('destroy');
        $('#tbl_citas').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,

            columns: [
                {
                    field: 'Fecha_Cita', title: 'Fecha cita',
                    width: '10%', align: 'center', sortable: true, formatter: function (value,
                    row) { return parsefecha(value); }
                },
                { field: 'No_Solicitud', title: 'No. Solicitud', width: '5%', align: 'center', valign: 'bottom', sortable: true, clickToSelect: false },
                 { field: 'Empleado_Visitado', title: 'Empleado Visitado', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Nombre_Solicitante', title: 'Nombre Solicitante', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },                     
                { field: 'Departamento', title: 'Departamento', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                {
                    field: 'Estatus', title: 'Estatus', width: '20%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {

                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "black";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'CONCLUIDO':
                                color = 'rgba(160, 160, 160,1)';
                                break;
                            case 'VALIDADO':
                                color = "orange";
                                break;
                            case 'RECHAZADO':
                                color = "Red";
                                break;
                            case 'ARRIVADO':
                                color = "rgba(232, 72, 38, 1)";
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'AUTORIZADO':
                                color = 'rgba(85, 255, 51, 1)';
                                break;
                            case 'FINALIZADO':
                                //color = "rgba(221, 24, 24, 1)";
                                color = "green";
                                break;
                            case 'MONITOREADO  FIN':
                                color = "rgba(40, 71, 120, 1)";
                                break;
                            case 'MONITOREADO  INICIO':
                                color = "rgba(27, 68, 133, 1)";
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }

                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
                  {
                      field: 'Fotos', title: 'Identificación', align: 'center', width: '2%',
                      formatter: function (value, row) {
                          return '<div>' +
                                      '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="_cargar_imagenes(this);"><i class="glyphicon glyphicon-picture" title=""></i></a>' +
                                   '</div>';
                      }

                  },
                  //{
                  //    field: 'Ruta', title: 'Ruta visitante', align: 'center', width: '2%',
                  //    formatter: function (value, row) {
                  //        return '<div>' +
                  //                    '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_ruta_click(this);"><i class="glyphicon glyphicon-map-marker" title=""></i></a>' +
                  //                 '</div>';
                  //    }

                  //},
                    {
                        field: 'Visualizar', title: 'Visualizar', align: 'center', width: '2%',
                        formatter: function (value, row) {
                            return '<div>' +
                                        '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-eye-open" title=""></i></a>' +
                                     '</div>';
                        }

                    },

                 {
                     field: 'Finalizar', title: 'Finalizar visita', align: 'center', width: '2%',
                     formatter: function (value, row) {
                         return '<div>' +
                                   '<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_finalizar_visita(this);"><i class="glyphicon glyphicon-ok"  title=""></i></a>' +
                                '</div>';
                     }
                 }                 
            ]
        });
    } catch (e) {

    }
}
function parseTimeHorario(timeString) {
    var dateTime = timeString.split(" ");
    var timeOnly = dateTime[0];
    var times = timeOnly.split(":");
    var temp = (times[0]) + ":" + times[1] + ":" + times[2];
    return temp;
}

function parseDateIntercambiarDiaMes(dateString) {
    //Intercambia el dia y el mes de los formatos de fecha( DD/MM/YYYY o MM/DD/YYYY )
    var dateTime = dateString.split(" ");
    var dateOnly = dateTime[0];
    var dates = dateOnly.split("/");
    var temp = dates[1] + "/" + dates[0] + "/" + dates[2];
    return temp;
}
function parsehora(timeString) {
    var txtHora = Date.parse(timeString).toString("hh:mm tt")
    return txtHora
    ;
}
function parsefecha(dateString) {
    var txtfecha = Date.parse(dateString).toString("dd/MM/yyyy")
    return txtfecha;

}
function _load_estatus() {
    var filtros = null;
    try {
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Estatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var datos_combo = $.parseJSON(datos.d);
                    var select2 = $('#cmb_estatus');
                    $('option', select2).remove();
                    var options = '<option value="">--SELECCIONE--</option>';
                    for (var Indice_Estatus = 0; Indice_Estatus < datos_combo.length; Indice_Estatus++) {
                        options += '<option value="' + datos_combo[Indice_Estatus].Estatus_ID + '">' + datos_combo[Indice_Estatus].Estatus.toUpperCase() + '</option>';
                        if (datos_combo[Indice_Estatus].Estatus.toUpperCase() == 'ABIERTO') {
                            estatusActivo = datos_combo[Indice_Estatus].Estatus_ID;
                        }
                    }
                    select2.append(options);
                }
            }
        });
    } catch (e) {

    }
}
function _crear_tbl_horarios() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('destroy');
        $('#tbl_lista_horarios').bootstrapTable({
            idField: 'Horario_Solicitud_Detalle_ID',
            uniqueId: 'Horario_Solicitud_Detalle_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Horario_Solicitud_Detalle_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'Fecha_Cita', title: 'Fecha Cita',
                    width: '30%', align: 'center', sortable: true, formatter: function (value,
                    row) { return parseDateIntercambiarDiaMes(value); }
                },
                {
                    field: 'Hora_Inicio', title: 'Hora inicio', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true, formatter: function (value,
                    row) { return parsehora(value); }
                },
              {
                  field: 'Hora_Fin', title: 'Hora fin', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true, formatter: function (value,
                      row) { return parsehora(value); }
              },
            ],
            onLoadSuccess: function (data) {
            }
        });
    }
    catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _agregar_lista_horarios() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('insertRow', {
            index: 0,
            row: {
                Fecha_Cita: parseDateIntercambiarDiaMes($('#f_cita').val()),
                Hora_Inicio: $('#h_inicio').val(),
                Hora_Fin: $('#h_termino').val(),

                No_Solicitud: $('txt_no_solicitud').val(),

            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _quitar_horario(horario) {
    var indexrow = null;
    var row = $(horario).data('horario');

    try {
        $('#tbl_lista_horarios').bootstrapTable('remove', {
            field: 'Hora_Inicio',
            field: 'Fecha_Cita',
            values: [$.trim(row.Hora_Inicio.toString()), $.trim(row.Fecha_Cita.toString())]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}

function _validar_datos_requeridos_agregar_horario() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#f_cita').val() == '' || $('#f_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una fecha para la cita.<br />';
        }

        if ($('#h_inicio').val() == '' || $('#h_inicio').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora de inicio para la cita.<br />';
        }
        if ($('#h_termino').val() == '' || $('#h_termino').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora fin para la cita.<br />';
        }
        if ($('#h_inicio').val() == $('#h_termino').val()) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La hora de inicio de la cita debe ser diferente a la hora fin.<br />';
        }
        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}
function _validar_duplicar_horarios(hora_inicio, fecha_cita) {
    var _horario_agregados = null;
    var _horario_existentes_tabla = false;
    try {
        _horario_agregados = $("#tbl_lista_horarios").bootstrapTable('getData');
        $.each(_horario_agregados, function (index, value) {
            if (value.Hora_Inicio == hora_inicio && value.Fecha_Cita == fecha_cita) {
                _horario_existentes_tabla = true;
            }

        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _horario_existentes_tabla;
}

function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _validar_datos() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';


        if ($('#cmb_estatus :selected').val() == '' || $('#cmb_estatus :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El estatus es un dato requerido.<br />';
        }
        if ($('cmb_departamento_visitado :selected').val() == '' || $('#cmb_departamento_visitado :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El departamento al que visitas es un campo requerido.<br />';
        }

        if ($("#nombre_solicitante").val() == null || $("#nombre_solicitante").val() == "") {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El nombre del solicitante es un campo requerido.<br />';
        }

        if ($('#apellidos_solicitante').val() == '' || $('#apellidos_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Los apellidos del solicitante son un campo requerido.<br />';
        }

        if ($('#txt_motivo_cita').val() == '' || $('#txt_motivo_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El motivo de la cita es un campo requerido.<br />';
        }

        if ($('#correo_solicitante').val() == '' || $('#correo_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu correo electrónico.<br />';
        }

        if ($('#telefono_solicitante').val() == '' || $('#telefono_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu teléfono<br />';
        }


        if ($table_horarios.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;No se ha agregado ningún horario para la cita.<br />';
        }

        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}

function btn_editar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
    if (row.Estatus == "CERRADO" || row.Estatus == "FINALIZADO" || row.Estatus == "CANCELADO") {
        _mostrar_mensaje('Solicitud Cita', 'No puedes modificar esta solicitud');
        return;
    }

    _habilitar_controles('Modificar');
    _crear_tbl_horarios();
    _consultar_horarios(row.No_Solicitud);
    // var usuario = _validar_usuario();
    // if (usuario)
    //  $('#cmb_estatus').attr({ disabled: false });
    // else
    // $('#cmb_estatus').attr("disabled", true);
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");




                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);
                    ////if (row.Estatus == "CANCELED") {
                    ////    $('#btn_guardar').css('display', 'none');
                    ////    $('#btn_salir').css('display', 'none');
                    ////    $('#btn_regresar').css('display', 'block');
                    ////}
                    ////if (row.Estatus == "CANCELED" || row.Estatus == "REJECTED") {
                    ////    $("#lbl_observaciones").css('display', 'block');
                    ////    $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    ////}
                    ////else {
                    ////    $("#lbl_observaciones").css('display', 'none');
                    ////}


                    // _load_estatus('#cmb_estatus');
                    _consultar_departamentos('#cmb_departamento_visitado');
                    _consultar_empleados1("#cmb_empleado_visitado");
                    _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _consultar_horarios(no_solicitud) {
    var filtros = null;
    try {

        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Horarios',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_horarios').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });

    } catch (e) {

    }
}

function _cargar_imagenes(renglon) {
    var row = $(renglon).data('solicitud_cita');

    if (row.Estatus == 'CANCELADO' || row.Estatus == 'RECHAZADO' || row.Estatus == 'AUTORIZADO') {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;No estan disponibles las imágenes de identificación para las citas con estatus: " + row.Estatus)
        return;
    }
    $('#div_principal_solicitud_citas').css('display', 'none');
    $('#div_imagenes').css('display', 'block');
   
    var filtros = null;
    var options = '';
    try {

        filtros = new Object();
        filtros.No_Solicitud = row.No_Solicitud;
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Imagenes',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var imagen = $.parseJSON(datos.d);
                    for (var Indice = 0; Indice < imagen.length; Indice++) {

                            if (numero_foto == 0) {

                                options += ' <div class="row">'
                            }

                            options +='<div class="col-xs-4 col-md-3" id="div_img">'
                            options += '<a href="#" class="thumbnail">'
                            options += '<img id="imagen" src=' + imagen[Indice].Ruta_Externa + ' class="img-thumbnail" alt="Responsive image">'
                            options +='</a>'
                            options += '<div id="overlay"></div></div>'
                            
                          
                            numero_foto++;

                            if(numero_foto>4){
                                options += '</div>'
                                numero_foto = 0;
                            } 
                        }
                        if (numero_foto != 0) {
                            options += '</div>'
                        }
                        options += '&nbsp;';
                        $('#div_fotos').append(options);
                    }
                }
            
        });

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}

function btn_visualizar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');

    $('#autorizacion_empleado').css('display', 'block');
    _habilitar_controles('Visualizar');
    _crear_tbl_horarios();
    _consultar_horarios(row.No_Solicitud);
    if (row.Estatus == 'CANCELADO' || row.Estatus=='RECHAZADO') {
        _consultar_horarios_visualizar(row.No_Solicitud);

    }
    $('#visualizar').css('display', 'block');
    $('#div_ruta').css('display','block')
    $('#divmapa').css('display', 'block')
    Repetir();
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
              
                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");

                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);

                    if (row.Estatus == "CANCELADO" || row.Estatus == "RECHAZADO") {
                        $("#lbl_observaciones").css('display', 'block');
                        $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    }
                    else {
                        $("#lbl_observaciones").css('display', 'none');
                    }
                    
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
        _consultar_ubicacion_vigilancia()
        _consultar_ubicacion_visitado();
        _consultar_ruta();
      
    
      

    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}

function btn_ruta_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
    $('#div_principal_solicitud_citas').css('display', 'none');
    $('#autorizacion_empleado').css('display', 'block');
    $('#visualizar').css('display', 'block');
    $('#div_ruta').css('display', 'block');
    $('#divmapa').css('display', 'block');
}
function btn_finalizar_visita(renglon) {
    var row = $(renglon).data('solicitud_cita');

    if (row.Estatus != "CONCLUIDO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;No puedes finalizar una visita con estatus: " + row.Estatus+".</br> La visita debe estar en estatus CONCLUIDO para poder finalizarla.");
        return;
    }
    bootbox.confirm({
        title: 'Finalizar visita',
        message: '¿Estás seguro de finalizar esta visita?',
        callback: function (result) {
            if (result) {
                _finalizar_visita(row.No_Solicitud);
                _search_citas();
            }
        }
    });
}
function _finalizar_visita(solicitud) {
    var horarios = null;
    var isComplete = false;
    try {
        horarios = new Object();
        horarios.No_Solicitud = parseInt(solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horarios) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Finalizar_Visita',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        //_consultar_horarios();
        // $('#tbl_lista_horarios').bootstrapTable('refresh', 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Horarios');
        //_search_alertas_rojas_por_filtros();
        //$('#tbl_lista_horarios').bootstrapTable('refresh', 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Horarios');
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;

}

function _search_solicitudes_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();

        if ($.trim($('#txt_busqueda_no_solicitud').val()) !== '')
            filtros.No_Solicitud = parseInt($('#txt_busqueda_no_solicitud').val());

        if ($.trim($('#txt_busqueda_solicitante').val()) !== '')
            filtros.Nombre_Solicitante = $('#txt_busqueda_solicitante').val();

        if ($.trim($('#txt_busqueda_apellidos').val()) !== '')
            filtros.Apellidos = $('#txt_busqueda_apellidos').val();

        if ($.trim($('#f_busqueda').val()) !== '')
            filtros.Fecha_Cita = $('#f_busqueda').val();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_citas').bootstrapTable('load', JSON.parse(datos.d));
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}

function _consultar_horarios_visualizar(no_solicitud) {
    var filtros = null;
    try {

        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Horarios_Visualizar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_horarios').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });

    } catch (e) {

    }
}