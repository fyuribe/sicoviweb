﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.Master" AutoEventWireup="true" CodeBehind="Frm_Ope_Seguimiento_Citas.aspx.cs" Inherits="web_cambios_procesos.Paginas.Operacion.Frm_Ope_Seguimiento_Citas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Recursos/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <link href="../../Recursos/estilos/demo_form.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-combo/select2.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table-current/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../../Recursos/icon-picker/css/icon-picker.css" rel="stylesheet" />
    <link href="../../Recursos/estilos/css_producto.css" rel="stylesheet" />
    <link href="../../Recursos/estilos/css_imagen.css" rel="stylesheet" />

    <script src="../../Recursos/plugins/parsley.js"></script>
    <script src="../../Recursos/bootstrap-table-current/bootstrap-table.js"></script>
    <script src="../../Recursos/plugins/jquery.formatCurrency-1.4.0.min.js"></script>
    <script src="../../Recursos/plugins/jquery.formatCurrency.all.js"></script>
    <script src="../../Recursos/plugins/accounting.min.js"></script>
    <script src="../../Recursos/plugins/pinch.js"></script>

    <%--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>--%>


    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <script src="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.js"></script>

    <script src="../../Recursos/bootstrap-table-current/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../Recursos/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="../../Recursos/bootstrap-table/locale/bootstrap-table-es-MX.min.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-editable.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>
    <script src="../../Recursos/icon-picker/js/iconPicker.js"></script>
    <script src="../../Recursos/plugins/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script src="../../Recursos/bootstrap-combo/select2.js"></script>
    <script src="../../Recursos/bootstrap-combo/es.js"></script>
    <script src="../../Recursos/plugins/URI.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../../Recursos/javascript/seguridad/Js_Controlador_Sesion.js"></script>
    <script src="../../Recursos/javascript/operacion/Js_Seguimiento_Citas.js"></script>

    <style>
        #tbl_ordenes_cambios_procesos thead tr th {
            border: none !important;
        }

            #tbl_ordenes_cambios_procesos thead tr th:nth-child(n+4) {
                border-left: 2px solid #ddd !important;
                border-right: 2px solid #ddd !important;
            }

        .select2-container {
            width: 100% !important;
        }

        .search input:first-of-type {
            min-width: 200px !important;
        }

        #div_img {
            display: inline-block;
            overflow: hidden;
            position: relative;
            background: #fFF;
        }

        #imagen {
            transition: all 1s ease-in-out;
        }

        #div_img:hover #imagen {
            -webkit-transform: scale(1.5);
            transform: scale(1.5);
            opacity: 0.9;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="div_principal_solicitud_citas">

        <div class="container-fluid" style="height: 100vh;">
            <div class="row">
                <div class="col-sm-12 text-left" style="background-color: white!important;">
                    <h3>Seguimiento de visitas</h3>
                </div>
            </div>

            <hr />
            <div class="panel panel-color panel-info collapsed" id="panel1">
                <div class="panel-heading filter">
                    <h3 class="panel-title">
                        <i style="color: white;" class="glyphicon glyphicon-filter"></i>&nbsp;Filtros de b&uacute;squeda
                    </h3>
                    <div class="panel-options">
                        <a id="ctrl_panel" href="#" data-toggle="panel">
                            <span class="collapse-icon">–</span>
                            <span class="expand-icon">+</span>
                        </a>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="fuente_lbl_controles" for="txt_busqueda_por_no_orden">No. Solicitud</label>
                            <input id="txt_busqueda_no_solicitud" type="text" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label class="fuente_lbl_controles" for="txt_busqueda_por_no_orden">Nombre(s) Solicitante</label>
                            <input id="txt_busqueda_solicitante" type="text" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label class="fuente_lbl_controles" for="txt_busqueda_por_no_orden">Apellidos(s) Solicitante</label>
                            <input id="txt_busqueda_apellidos" type="text" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label class="text-bold text-left text-medium fuente_lbl_controles" title="Fecha Inicio" style="margin-bottom: 5px !important;">Fecha Cita</label>
                            <div class="input-group date" id="dtp_fecha_busqueda">
                                <input type="text" id="f_busqueda" class="form-control" style="margin: 0px;" placeholder="dd/mm/aaaa" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 18px !important; text-align: right !important;">
                            <button type="button" id="btn_busqueda" class="btn btn-secondary btn-icon btn-icon-standalone btn-lg" style="width: 40% !important;">
                                <i class="fa fa-search"></i>
                                <span>Buscar</span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <hr />
            <div id="toolbar" style="margin-left: 4px; text-align: right;">
                <div class="btn-group" role="group" style="margin-left: 4px;">
                    <button id="btn_inicio" type="button" class="btn btn-info btn-sm" style="border-radius: 0px !important; border-top-left-radius: 6px !important; border-bottom-left-radius: 6px !important;" title="Inicio"><i class="glyphicon glyphicon-home"></i></button>
                </div>
            </div>
            <table id="tbl_citas" data-toolbar="#toolbar" class="table table-responsive"></table>
        </div>
    </div>
    <div id="div_Informacion" style="display: none">
        <div id="div_orden_cambio_proceso">
            <div class="container-fluid" style="height: 100vh;">
                <div id="div_solo_informacion">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-8 text-left" style="background-color: white!important;">
                                <h3>Información de la cita</h3>
                                <label id="lbl_observaciones" style="display: none; color: red; font-style: italic; font-size: medium"></label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="text-align: right">
                                    <button id="btn_regresar" type="button" class="btn btn-primary btn-sm" title="">
                                        <i class="glyphicon glyphicon-arrow-left"></i>&nbsp;&nbsp;Regresar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--Contenedor de rows de formulario--%>

                    <div class="row">

                        <div class="col-md-2 col-xs-2">
                            <label for="txt_no_solicitud" class="fuente_lbl_controles">&nbsp;&nbsp;No. Solicitud</label>
                            <input id="txt_no_solicitud" type="text" class="form-control" disabled="disabled" />
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <label for="cmb_estatus" class="fuente_lbl_controles">(*)Estatus:</label>
                            <select id="cmb_estatus" class="form-control"></select>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <label for="nombre_solicitante" class="fuente_lbl_controles">(*)Nombre(s) del solicitante</label>
                            <input id="nombre_solicitante" type="text" class="form-control" placeholder="Nombre(s)" />
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <label for="apellidos_olicitante" class="fuente_lbl_controles">(*)Apellido(s) del solicitante</label>
                            <input id="apellidos_solicitante" type="text" class="form-control" placeholder="Apellido(s)" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <label class="fuente_lbl_controles">(*)Medio Informativo:</label>

                        </div>
                        <div class="col-md-4 col-xs-4">
                            <label class="fuente_lbl_controles">Correo</label>
                            <input type="text" id="correo_solicitante" class="form-control" placeholder="Correo electrónico" />
                        </div>

                        <div class="col-md-4 col-xs-4">
                            <label class="fuente_lbl_controles">Teléfono</label>
                            <input type="text" id="telefono_solicitante" class="form-control" placeholder="Télefono" onkeypress="return _solo_numeros(event)" maxlength="15" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <label for="cmb_departamento_visitado" class="fuente_lbl_controles">(*) Departamento</label>
                            <select id="cmb_departamento_visitado" class="form-control"></select>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <label for="cmb_empleado_visitado" class="fuente_lbl_controles">(*)Visita a:</label>
                            <select id="cmb_empleado_visitado" class="form-control"></select>
                        </div>
                    </div>
                    <div class="row"></div>
                    </br>
                            <div class="row">
                                <div class="table-responsive">
                                    <table id="tbl_lista_horarios" class="table table-responsive"></table>
                                </div>
                            </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label class="fuente_lbl_controles">(*) Descripción del motivo de la cita</label>
                            <textarea id="txt_motivo_cita" class="form-control input-sm" rows="5" placeholder="Motivo de la cita" data-parsley-required="true" maxlength="250" style="min-height: 50px !important;"></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <%--</div>--%>
            <div class="row space"></div>
        </div>
    </div>
    <div id="div_imagenes" style="display: none">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-8 text-left" style="background-color: white!important;">
                    <h3>Imágenes de indenticación</h3>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="text-align: right">
                        <button id="btn_volver" type="button" class="btn btn-primary btn-sm">
                            <i class="glyphicon glyphicon-arrow-left"></i>&nbsp;&nbsp;Regresar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="div_fotos">
        <div class="row">
        </div>
    </div>
    <div id="div_ruta" style="display: none">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-8 text-left" style="background-color: white!important;">
                    <h3>Ruta del visitante </h3>
                </div>
            </div>
        </div>
    </div>
    <div id="divmapa" style="position: relative; width: 100%; height: 600px; overflow: visible; display: none;"></div>

    <script>
        var mapa;

        function iniciar(vigilancia) {

            var origen = {
                lat: 20.686216,
                lng: -101.354958
            };

            var div = document.getElementById('divmapa')
            mapa = new google.maps.Map(div, {
                center: origen,
                zoom: 16
            });

            var ubicacionVigilancia = vigilancia;

            marker = new google.maps.Marker({
                position: ubicacionVigilancia,
                map: mapa
            });

        }

        function dibujar_puntos(ruta) {

            var puntos = ruta;

            var linea = new google.maps.Polyline({
                path: puntos,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            })

            linea.setMap(mapa);
        }

        function dibujar_puntos_regreso(ruta) {

            var puntos = ruta;

            var linea = new google.maps.Polyline({
                path: puntos,
                geodesic: true,
                strokeColor: '#00ff00',
                strokeOpacity: 1.0,
                strokeWeight: 2
            })

            linea.setMap(mapa);
        }

        function ubicacion_visitado(ubicacion) {
            var ubicacion = ubicacion;

            marker = new google.maps.Marker({
                position: ubicacion,
                animation: google.maps.Animation.BOUNCE
            });
            marker.setMap(mapa);

        }

        function _consultar_ruta() {
            var filtros = null;
            try {

                filtros = new Object();
                filtros.Solicitud_ID = parseInt($('#txt_no_solicitud').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Ruta',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {

                            var ruta = $.parseJSON(datos.d);
                            var miobjeto = [];
                            for (var Indice = 0; Indice < ruta.length; Indice++) {

                                miobjeto.push({ lat: ruta[Indice].Latitud, lng: ruta[Indice].Longitud });
                            }
                        }

                        setInterval(dibujar_puntos(miobjeto), 3000);
                    }

                });

            } catch (e) {

            }
        }

        function _consultar_ruta_regreso() {
            var filtros = null;
            try {

                filtros = new Object();
                filtros.Solicitud_ID = parseInt($('#txt_no_solicitud').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Ruta_Regreso',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {

                            var ruta = $.parseJSON(datos.d);
                            var miobjeto = [];

                            for (var Indice = 0; Indice < ruta.length; Indice++) {

                                miobjeto.push({ lat: ruta[Indice].Latitud, lng: ruta[Indice].Longitud });
                            }
                        }
                        setInterval(dibujar_puntos_regreso(miobjeto), 3000);
                    }

                });

            } catch (e) {

            }
        }

        function _consultar_ubicacion_visitado() {
            var filtros = null;
            try {

                filtros = new Object();
                filtros.No_Solicitud = parseInt($('#txt_no_solicitud').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Ubicacion_Visitado',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {
                            var ruta = $.parseJSON(datos.d);
                            var objeto;

                            for (var Indice = 0; Indice < ruta.length; Indice++) {

                                objeto = { lat: ruta[Indice].Latitud_Visitado, lng: ruta[Indice].Longitud_Visitado };
                            }
                        }
                        ubicacion_visitado(objeto);
                    }

                });

            } catch (e) {

            }
        }

        function _consultar_ubicacion_vigilancia() {
            var filtros = null;
            try {

                filtros = new Object();
                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Seguimiento_Citas_Controller.asmx/Consultar_Ubicacion_Vigilancia',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {

                            var ruta = $.parseJSON(datos.d);
                            var ubicacionvigilancia;


                            for (var Indice = 0; Indice < ruta.length; Indice++) {

                                ubicacionvigilancia = { lat: ruta[Indice].Latitud_Ubicacion_Vigilancia, lng: ruta[Indice].Longitud_Ubicacion_Vigilancia };
                            }
                        }
                        iniciar(ubicacionvigilancia);
                    }

                });

            } catch (e) {

            }
        }

        function Repetir() {
            setInterval(_consultar_ruta, 15000); //multiplicas la cantidad de segundos por mil
            // setInterval(_consultar_ruta_regreso, 15000);
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpJMhd5A-jAGTq9ICV61eexuA1oOhs8CY&callback=iniciar"></script>

</asp:Content>

