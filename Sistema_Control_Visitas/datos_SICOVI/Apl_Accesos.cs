//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace datos_cambios_procesos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Apl_Accesos
    {
        public int Rol_ID { get; set; }
        public int Menu_ID { get; set; }
        public int Estatus_ID { get; set; }
        public string Habilitado { get; set; }
        public string Alta { get; set; }
        public string Cambio { get; set; }
        public string Eliminar { get; set; }
        public string Consultar { get; set; }
        public string Usuario_Creo { get; set; }
        public string Ip_Creo { get; set; }
        public string Equipo_Creo { get; set; }
        public Nullable<System.DateTime> Fecha_Creo { get; set; }
    
        public virtual Apl_Menus Apl_Menus { get; set; }
        public virtual Apl_Estatus Apl_Estatus { get; set; }
    }
}
